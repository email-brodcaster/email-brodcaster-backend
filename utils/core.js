exports.ServerError = class ServerError extends Error {
  // eslint-disable-next-line require-jsdoc
  constructor(message, status) {
    super(message);
    this.status = status;
  }
};
