# Base image
FROM node:12
# Make folder to put our files in
WORKDIR /usr/app
# Copy package json and install dependencies
COPY package*.json ./
# Copy our app
COPY . .

RUN npm i
RUN npm run lint
RUn npm run test
# Command to run our app
CMD [ "npm", "run", "dev"]

