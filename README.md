# Email Prodcaster Server

Email brodcast portal using MailJet.

## perquisites

- nodejs v12
- MailJet [account](https://app.mailjet.com/)

## Installation

- clone this repo

```shell
git clone https://gitlab.com/email-brodcaster/email-brodcaster-backend.git
```

- install dependencies

```shell
npm i
```

- Add you env variables

```shell
cp .env.example .env
```

## Start the server

- starting the server in development mode

```shell
npm run dev
```

- starting the server in deployment mode

```shell
npm start
```

## Run using docker

- Run full-stack by cloning the [frontend-repo](https://gitlab.com/email-brodcaster/email-brodcaster-frontend). Make sure to put the `email-brodcaster-frontend/` and the `email-brodcaster-backend/` on the same directory

- Copy [docker-compose.yml](./docker-compose.yml)

```shell
cp docker-compose.yml ..
cd ..
```

- Run the full-stack project using only one command

```shell
docker-compose up -d
```

- Pull images from [dockerHub](https://hub.docker.com/r/yehia67/emailbrodcaster/)

- For the deployed server check the [url](http://54.209.112.29:8080/)

## Documentation

- [Postman collection](https://documenter.getpostman.com/view/4810649/TW73G6vC)
