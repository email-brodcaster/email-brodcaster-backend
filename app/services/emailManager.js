const fs = require('fs');
const mustache = require('mustache');
const mjml = require('mjml');

//  jetEmail client
const { emailJet } = require('../../config');

const { emailJetApiKey, emailJetSecreteKey, validatedEmail } = emailJet;
const mailjet = require('node-mailjet').connect(
  emailJetApiKey,
  emailJetSecreteKey
);

exports.brodcast = async ({ body }) => {
  try {
    const {
      recivers,
      subject,
      messageContent,
      senderName,
      emailTemplate = 'defaultMessage',
    } = body;
    // upload template
    const path = require('path').resolve(
      `./app/public/mail-templates/${emailTemplate}.mjml`
    );
    const mjmlTemplate = fs.readFileSync(path, 'utf8');

    const response = await Promise.all(
      recivers.map(async (reciver) => {
        const renderedMJML = await mustache.render(mjmlTemplate, {
          user: reciver.Name,
          messageContent,
          senderName,
        });
        const htmlTemplate = await mjml(renderedMJML).html;
        const result = await sendEmail(reciver, subject, htmlTemplate);
        if (result.err) {
          return {
            err: 'something went wrong',
            status: result.status,
          };
        }
        return result.body;
      })
    );
    const error = response.filter((mail) => {
      if (mail.err) return mail.status;
    });
    if (error[0]) {
      return {
        err: 'something went wrong',
        status: 500,
      };
    }
    return {
      message: 'mails sent successfully',
      data: response,
    };
  } catch (error) {
    return {
      err: error,
      status: 500,
    };
  }
};

const sendEmail = async (to, subject, htmlTemplate) => {
  try {
    const request = await mailjet.post('send', { version: 'v3.1' }).request({
      Messages: [
        {
          From: {
            Email: validatedEmail,
            Name: 'Yehia',
          },
          To: [to],
          Subject: subject,
          HTMLPart: htmlTemplate,
        },
      ],
    });
    return request;
  } catch (error) {
    return {
      err: 'something went wrong',
      status: error.statusCode,
    };
  }
};
