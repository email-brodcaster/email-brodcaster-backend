const chai = require('chai');
const chaiHttp = require('chai-http');

const { app } = require('../index');

const expect = chai.expect;

chai.use(chaiHttp);

describe('Test client user storie', () => {
  it('should get server up and running', (done) => {
    chai
      .request(app)
      .get('/')
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });
  it('should send email to 2 users', (done) => {
    chai
      .request(app)
      .post('/mail/all')
      .send({
        recivers: [
          {
            Email: 'yehiatarek1997@gmail.com',
            Name: 'yehia97',
          },
          {
            Email: 'yehiatarekabdelmonem@gmail.com',
            Name: 'yehiatarekabdelmon',
          },
        ],
        subject: 'unit test case',
        messageContent: 'test content description inshallah.',
        senderName: 'Yehia Tarek',
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });
});
