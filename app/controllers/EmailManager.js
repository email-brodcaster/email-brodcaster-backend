const { brodcast } = require('../services/emailManager');

const { controller } = require('../middleware/controller');

module.exports = {
  brodcast: controller(brodcast),
};
