exports.brodcastSchema = {
  type: 'object',
  required: ['body'],
  properties: {
    body: {
      required: ['recivers', 'subject', 'messageContent', 'senderName'],
      properties: {
        recivers: {
          type: 'array',
          items: {
            type: 'object',
            required: ['Name', 'Email'],
            properties: {
              Name: {
                type: 'string',
                minLength: 3,
                maxLength: 35,
                errorMessage:
                  'reciver name should be a string between 3 and 35 characters',
              },
              Email: {
                type: 'string',
                format: 'email',
                errorMessage: 'email should be a valid email',
              },
              additionalProperties: false,
            },
          },
        },
        subject: {
          type: 'string',
          minLength: 3,
          maxLength: 255,
          errorMessage:
            'subject should be a string between 3 and 255 characters',
        },
        messageContent: {
          type: 'string',
          minLength: 3,
          errorMessage:
            'message content should be a string with at least 3 character',
        },
        senderName: {
          type: 'string',
          minLength: 3,
          maxLength: 35,
          errorMessage:
            'sender name should be a string between 3 and 35 characters',
        },
      },
      additionalProperties: false,
      errorMessage: {
        required: {
          recivers: 'recivers are required',
          subject: 'subject is required',
          messageContent: 'message content is required',
          senderName: 'sender name is required',
        },
      },
    },
    params: {},
    query: {},
  },
  errorMessage: {
    required: {
      query: 'body object is required',
    },
  },
};
