const router = require('express-promise-router')();

const { validate } = require('../middleware/validator');
const { brodcastSchema } = require('../requestSchema/mail/bordcast');

const { brodcast } = require('../controllers/EmailManager');

router.post('/all', validate(brodcastSchema), brodcast);

exports.mailRouter = router;
