const dotenv = require('dotenv');

const envFound = dotenv.config();
if (!envFound) {
  throw new Error(" Couldn't find .env file!");
}

module.exports = {
  port: parseInt(process.env.PORT, 10),
  api: {
    prefix: '/',
  },
  emailJet: {
    emailJetApiKey: process.env.MJ_APIKEY_PUBLIC,
    emailJetSecreteKey: process.env.MJ_APIKEY_PRIVATE,
    emailJetHost: process.env.MJ_HOST,
    emailJetPort: process.env.MJ_PORT,
    validatedEmail: process.env.MJ_VALIDATED_EMAIL,
  },
  dbUserName: process.env.DB_USERNAME,
  dbPassword: process.env.DB_PASSWORD,
  dbName: process.env.DB_NAME,
  dbHost: process.env.DB_HOST,
  dbPort: process.env.DB_PORT,
  dbDialect: process.env.DB_Dilcate,
  jwtSecret: process.env.JWT_SECRET,
  serverUrl: process.env.SERVER_URL,
  jwtSecret: process.env.JWT_SECRET,
};
